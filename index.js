// alert("Hello World");

/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1

*/

function register(user){

    if (registeredUsers.includes(user)) {
        alert("Registration failed. Username already exists!");
    } else {
        registeredUsers.push(user);
        alert("Thank you for registering!");
    }   
};

/*
    2.

*/

function addFriend(foundUser) {

    if (registeredUsers.includes(foundUser)) {
        friendsList.push(foundUser);
        alert("You have added " + foundUser + " as a friend!");
    } else {
        alert("User not found.");
    }
};


/*
    3. 
*/

function displayFriend() {

    if (friendsList.length === 0) {
        alert("You currently have 0 friends. Add one first.");
    } else {
        friendsList.forEach(function(friend) {
            console.log(friend);
        });
    }
};

/*
    4. 
*/

function displayNumberOfFriends() {

    if (friendsList.length === 0) {
        alert("You currently have 0 friends. Add one first.");
    } else {
        alert("You currently have " + friendsList.length + " friend(s).");
    }
};


/*
    5. 
*/

function deleteFriend() {

    if (friendsList.length === 0) {
        alert("You currently have 0 friends. Add one first.");
    } else {
        friendsList.pop();
    }
};
